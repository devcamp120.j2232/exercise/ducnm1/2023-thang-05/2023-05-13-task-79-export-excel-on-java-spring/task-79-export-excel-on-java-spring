export function setDatasource(dataTable, datasource) {
    dataTable.clear();
    dataTable.rows.add(datasource);
    dataTable.draw();
}