export async function downloadFile(url) {
    try {
        var response = await fetch(url);
        console.log(response);

        if (!response.ok) {
            throw new Error("error downloading file");
        }

        var blob = await response.blob();
        console.log("blob: ", blob);
        const disposition = response.headers.get("Content-Disposition");
        console.log("response.headers: ", response.headers);
        console.log("disposition: ", disposition);
        let fileName = "exported_file.xlsx";

        if (disposition && disposition.includes("filename")) {
            const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
            const matches = filenameRegex.exec(disposition);

            if (matches !== null && matches[1]) {
                fileName = matches[1].replace(/['"]/g, "");
            }
        }

        const urlObject = window.URL.createObjectURL(blob);
        const a = document.createElement("a");
        a.href = urlObject;
        console.log(fileName);
        a.download = fileName || "exported_file.xlsx";
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        window.URL.revokeObjectURL(urlObject);

    } catch (error) {
        console.error(error);
        alert("Error: " + error.message);
    }
}