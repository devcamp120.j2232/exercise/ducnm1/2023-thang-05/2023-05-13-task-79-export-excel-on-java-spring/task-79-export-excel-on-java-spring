import * as downloader from "./downloader.js";
import * as dataTableHelper from "./datatable-utils.js";

$(document).ready(function () {
    //initDataTable();
    loadOrderData();
    addEventListener();
});

function addEventListener() {
    $("#btn-export-excel").on("click", function () {
        onBtnExportExcelClick();
    });
}

function onBtnExportExcelClick() {
    downloader.downloadFile("http://localhost:8080/order/export/excel");
}

function initDataTable() {
    $("#main-table").dataTable({
        columns: [
            {
                render: function (data, type, row, meta) {
                    return meta.row + 1;
                }
            },
            { data: "id", defaultContent: "" },
            { data: "orderDate", defaultContent: "" },
            { data: "requiredDate", defaultContent: "" },
            { data: "shippedDate", defaultContent: "" },
            { data: "status", defaultContent: "" },
            { data: "comments", defaultContent: "" }
        ],
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#main-table_wrapper .col-md-6:eq(0)');
}

async function loadOrderData() {
    let response = await fetch("http://localhost:8080/order/all");
    if (!response.ok) {
        throw new Error("error loading order");
    }

    let data = await response.json();
    dataTableHelper.setDatasource($("#main-table").DataTable(), data);
    console.log(data);


}