package com.devcamp.pizza365.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.config.ReactiveQuerydslWebConfiguration;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.repository.OrderRepository;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    public byte[] exportOrdersToExcel() throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Orders");
        List<Order> orders = orderRepository.findAll();

        addRow(sheet, 0, 0, "id", "order date", "order shipped date", "order status", "order comments", "customer id");
        for (int i = 0; i < orders.size(); i++) {
            Order order = orders.get(i);
            int rowIndex = i + 1;
            int columnIndex = 0;
            addRow(sheet, rowIndex, columnIndex++,
                    order.getId(),
                    order.getOrderDate(),
                    order.getShippedDate(),
                    order.getStatus(),
                    order.getComments(),
                    order.getCustomer().getId());
        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        workbook.write(outputStream);
        workbook.close();

        return outputStream.toByteArray();

    }

    public void addRow(XSSFSheet sheet, int rowIndex, int startColumnIndex, Object... objects) {
        Row row = sheet.createRow(rowIndex);
        int objectIndex = 0;
        for (int columnIndex = startColumnIndex; columnIndex < objects.length; columnIndex++) {
            Cell cell = row.createCell(columnIndex);
            cell.setCellValue(objects[objectIndex++].toString());
            // setCellValueCore(cell, objects[objectIndex++].toString());
            // objectIndex++;
        }

    }

    public void setCellValueCore(Cell cell, Object value) {
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }
    }
}
